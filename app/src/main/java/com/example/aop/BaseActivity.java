package com.example.aop;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.Nullable;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

public class BaseActivity extends Activity implements LoginCheck {
    private static final String TAG = OrderActivity.class.getSimpleName();
    protected LoginCheck mLoginCheck;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mLoginCheck = (LoginCheck) Proxy.newProxyInstance(LoginCheck.class.getClassLoader(), new Class[]{LoginCheck.class}, new LoginHandler(this));
    }


    @Override
    public boolean isLogin() {
        if (BaseApplication.isLogin) {
            return true;
        }
        return false;
    }

    private class LoginHandler implements InvocationHandler {
        private LoginCheck loginCheck;

        public LoginHandler(LoginCheck loginCheck) {
            this.loginCheck = loginCheck;
        }

        @Override
        public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
            if (loginCheck != null) {
                if (isLogin()) {
                    Log.e(TAG, "当前是登陆状态，继续进行下一步业务...");
                    Toast.makeText(BaseActivity.this, "当前是登陆状态，继续进行下一步业务...", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(BaseActivity.this, "当前未登陆，进行登陆...", Toast.LENGTH_SHORT).show();
                    Log.e(TAG, "未登录，跳转登陆页面");
                    startActivity(new Intent(BaseActivity.this, LoginActivity.class));
                }
                return method.invoke(loginCheck, args);
            }
            return null;
        }
    }
}
