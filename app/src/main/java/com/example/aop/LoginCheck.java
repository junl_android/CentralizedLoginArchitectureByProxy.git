package com.example.aop;

public interface LoginCheck {
    boolean isLogin();
}
