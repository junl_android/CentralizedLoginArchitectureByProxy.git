package com.example.aop;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.annotation.Nullable;


public class OrderActivity extends BaseActivity {

    private Button btnLoginState;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order);
        btnLoginState = findViewById(R.id.login_state);
        btnLoginState.setText("当前登录状态：" + BaseApplication.isLogin);
    }

    public void queryMyOrder(View view) {
        // 一行代码实现： 是否登录状态，如是继续业务，否则跳转登录。
        if (mLoginCheck != null) mLoginCheck.isLogin();
    }

    public void resetLoginState(View view) {
        if (BaseApplication.isLogin) {
            BaseApplication.isLogin = false;
            btnLoginState.setText("当前登录状态：false");
        } else {
            BaseApplication.isLogin = true;
            btnLoginState.setText("当前登录状态：true");
        }
    }
}
