package com.example.aop;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

public class MainActivity extends AppCompatActivity implements DBOperation{
    private DBOperation dbOperation;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        dbOperation = (DBOperation) Proxy.newProxyInstance(DBOperation.class.getClassLoader(), new Class[]{DBOperation.class}, new DBHandler(this));
    }

    public void insert(View view) {
        dbOperation.insert();
    }

    public void delete(View view) {
        dbOperation.delete();
    }

    public void update(View view) {
        dbOperation.update();
    }

    public void jumpOrder(View view) {
        startActivity(new Intent(MainActivity.this, OrderActivity.class));
    }

    public void jumpShopping(View view) {
        startActivity(new Intent(MainActivity.this, ShoppingActivity.class));
    }


    private class DBHandler implements InvocationHandler {
        DBOperation dbOperation;
        public DBHandler(DBOperation  db) {
            dbOperation = db;
        }

        @Override
        public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
            if (dbOperation != null) {
                Log.e("TAG", "操作数据库之前，开始备份数据库 ");
                save();
                Log.e("TAG", "数据库备份完成后，等待后续操作...");
                return method.invoke(dbOperation, args);
            }
            return null;
        }
    }

    @Override
    public void insert() {
        Log.e("TAG", "insert ");
    }

    @Override
    public void delete() {
        Log.e("TAG", "delete");
    }

    @Override
    public void update() {
        Log.e("TAG", "update");
    }

    @Override
    public void query() {

    }

    @Override
    public void save() {
        Log.e("TAG", "备份数据...");
    }

}
