package com.example.aop;

public interface DBOperation {
    void insert();

    void delete();

    void update();

    void query();

    /**
     * 每次操作数据库之前进行数据备份
     */
    void save();

}
