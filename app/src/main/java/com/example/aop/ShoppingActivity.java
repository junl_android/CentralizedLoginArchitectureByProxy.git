package com.example.aop;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;

public class ShoppingActivity extends BaseActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shopping);
    }

    public void shopping(View view) {
        if (mLoginCheck != null) mLoginCheck.isLogin();
    }
}
